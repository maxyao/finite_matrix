package matrix;

abstract class Operable<T extends Operable> {

    abstract T add(T other);

    abstract void dAdd(T other);

    abstract T sub(T other);

    abstract T mul(T other);

    abstract void dMul(T other);

    abstract T div(T other);

    abstract T inverse();
}
