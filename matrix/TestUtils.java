package matrix;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static matrix.Utils.*;

public class TestUtils {

    @Test
    public void testGCD() {
        assertEquals(GCD(1, 3), 1);
        assertEquals(GCD(4, 4), 4);
        assertEquals(GCD(0, 4), 4);
    }
}
