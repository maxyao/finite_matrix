package matrix;

import static matrix.Utils.*;

/** Rational Operable class representing all numbers that can be
 *  written as a fraction of two integers. For convenience's sake,
 *  negative numbers have are all represented as: "-a/b", positive numbers
 *  are just "a/b".
 *  @author Max Yao
 */

class Rational extends Operable<Rational> {

    /** The numerator of the number. */
    private int numerator;

    /** The denominator of the number. */
    private int denominator;

    /** A boolean that indicates whether the number is positive or
     *  negative. If positive, true, else, false. positive numbers'
     *  plus sign is not printed! */
    private boolean positive;

    /** Constructor for this class. If a number is a fraction, provide numerator
     *  and denominator.
     * @param numerator
     * @param denominator
     */
    Rational(int numerator, int denominator) {
        if (denominator == 0 && numerator != 0) {
            throw new ArithmeticException("Denominator can NOT be 0!");
        }
        if (denominator < 0) {
            throw new IllegalArgumentException("If you want to put in a negative number, "
                    + "change numerator to zero.");
        }
        if (numerator == 0) {
            this.numerator = numerator;
            this.denominator = 1;
            this.positive = true;
        } else {
            this.numerator = numerator;
            this.denominator = denominator;
            simplify();
            this.positive = numerator >= 0;
        }
    }

    /** Constructor for this class. If a number is an integer/whole-number,
     *  the denominator is default to 1.
     * @param wholeNumber
     */
    Rational(int wholeNumber) {
        this(wholeNumber, 1);
    }

    /** Negate the sign of this object. */
    void dNegate() {
        numerator = -numerator;
    }

    /** Negate the sign but returns a new object. */
    Rational negate() {
        return new Rational(-numerator, denominator);
    }

    /** Add this Rational to another Rational.
     * @param other
     * @return a new Rational object
     */
    @Override
    Rational add(Rational other) {
        if (denominator == 1 && other.getDenominator() == 1) {
            return new Rational(numerator + other.getNumerator());
        }
        int otherNum = other.getNumerator();
        int otherDen = other.getDenominator();
        int newNum = numerator * otherDen + otherNum * denominator;
        int newDen = denominator * otherDen;
        return new Rational(newNum, newDen);
    }

    /** Add another Rational to this Rational, destructively.
     * @param other
     */
    @Override
    void dAdd(Rational other) {
        if (denominator == 1 && other.getDenominator() == 1) {
            numerator += other.getNumerator();
        } else {
            int otherNum = other.getNumerator();
            int otherDen = other.getDenominator();
            int newNum = numerator * otherDen + otherNum * denominator;
            int newDen = denominator * otherDen;
            numerator = newNum;
            denominator = newDen;
            positive = newNum >= 0;
        }
    }

    /** Subtract this Rational from another Rational.
     * @param other
     * @return a new Rational object
     */
    @Override
    Rational sub(Rational other) {
        return add(other.negate());
    }

    /** Multiply this Rational with another Rational.
     * @param other
     * @return a new Rational object
     */
    @Override
    Rational mul(Rational other) {
        int newNum = numerator * other.getNumerator();
        int newDen = denominator * other.getDenominator();
        return new Rational(newNum, newDen);
    }

    /** Multiply this Rational with another Rational destructively.
     * @param other
     */
    @Override
    void dMul(Rational other) {
        int otherNum = other.getNumerator();
        int otherDen = other.getDenominator();
        if (otherDen == 1) {
            numerator *= otherNum;
        } else {
            numerator *= otherNum;
            denominator *= otherDen;
            simplify();
        }
        positive = numerator >= 0;
    }

    /** Divide this Rational by another Rational.
     * @param other
     * @return a new Rational object
     */
    @Override
    Rational div(Rational other) {
        if (other.getNumerator() == 0) {
            throw new ArithmeticException("Can't divide by a zero number.");
        }
        int newNum = Math.abs(numerator * other.getDenominator());
        int newDen = Math.abs(denominator * other.getNumerator());
        if ((positive && other.isPositive()) || (!positive && !other.isPositive())) {
            return new Rational(newNum, newDen);
        }
        return new Rational(-newNum, newDen);
    }

    /** Returns the inverse Rational to this Rational.
     *  Inverse is defined as a Rational when multiplied with this
     *  Rational, the result is equivalent to One.
     * @return A new Rational that would equal to One when multiplied with
     * this Rational
     */
    @Override
    Rational inverse() {
        if (positive) {
            return new Rational(denominator, numerator);
        }
        return new Rational(-denominator, -numerator);
    }

    /** Get the numerator field.
     * @return numerator
     */
    int getNumerator() {
        return numerator;
    }

    /** Get the denominator field.
     * @return denominator
     */
    int getDenominator() {
        return denominator;
    }

    /** Get the positive field.
     * @return positive
     */
    boolean isPositive() {
        return positive;
    }


    /** Simplify a fraction to it's simplest form. */
    void simplify() {
        int gcd = GCD(Math.abs(numerator), Math.abs(denominator));
        if (gcd != 1) {
            numerator /= gcd;
            denominator /= gcd;
        }
    }

    /** Put the object in String form. Positive numbers' plus sign is ignored,
     *  for negative numbers, a '-' is printed in front of the number itself.
     * @return string form of this object
     */
    public String toString() {
        if (denominator == 1) {
            return String.format("%d", numerator);
        }
        if (numerator == 0) {
            return "0";
        }
        return String.format("%d/%d", numerator, denominator);
    }

    /** Returns true if two Rationals are equal.
     * @param other
     * @return true two Rationals are equal, false otherwise.
     */
    public boolean equals(Rational other) {
        simplify();
        other.simplify();
        return numerator == other.getNumerator()
                && denominator == other.getDenominator();
    }
}
