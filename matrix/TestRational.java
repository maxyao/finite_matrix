package matrix;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestRational {

    @Test
    public void testToString() {
        Rational n = new Rational(10, 5);
        assertTrue(n.toString().equals("2"));

        Rational n2 = new Rational(3, 9);
        assertTrue(n2.toString().equals("1/3"));

        Rational n3 = new Rational(-4, 16);
        assertTrue(n3.toString().equals("-1/4"));

        Rational n4 = new Rational(10);
        assertTrue(n4.toString().equals("10"));
    }

    @Test
    public void testSimplify() {
        Rational a = new Rational(0, 3);
        a.simplify();
    }

    @Test
    public void testEquals() {
        Rational a = new Rational(1, 3);
        Rational b = new Rational(3, 9);
        assertTrue(a.equals(b));

        Rational a1 = new Rational(6);
        Rational b1 = new Rational(-6);
        assertTrue(!a1.equals(b1));

        Rational a2 = new Rational(0, 9);
        Rational b2 = new Rational(0, 3);
        assertTrue(a2.equals(b2));
    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testDAdd() {

    }

    @Test
    public void testSub() {

    }

    @Test
    public void testMul() {

    }

    @Test
    public void testDMul() {

    }

    @Test
    public void testDiv() {

    }
}
