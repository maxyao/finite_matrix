package matrix;

class Matrix<E extends Operable> {

    private Row[] rows;

    private int row_size;

    private int col_size;

    public Matrix(Row[] rows) {
        int desinatedSize = rows[0].size();
        for (Row r : rows) {
            if (r.size() != desinatedSize) {
                throw new AssertionError("Every row must have"
                        + "the same number of entries!");
            }
        }
        this.rows = rows;
        this.row_size = rows.length;
        this.col_size = rows[0].size();
    }
}
